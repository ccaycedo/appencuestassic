## APLICATIVO ENCUESTAS SIC CRISTHIAN CAYCEDO

# Base de datos mysql

La base de datos esta en el mpotor mysql con usuario root y la clave esta vacia:
###### Conexion al motor: Base de datos: encuestas_sic, Usuario: root , Clave: (VACIA)

![picture](img/basdat.png)

Tablas creadas para almacenamiento de datos: Tabla usuario para controlar el accso al sistema, tabla encuestas donde se almacenan todas la encuestas diligenciadas, tabla marcas donde se parametrizan todas las marcas de conputadores.

![picture](img/a.png)
![picture](img/b.png)
![picture](img/c.png)

# Backoffice en Netbeans

Para el back office se uso Netbeans JPA+ JSF con la siguiente estructura de capas:

![picture](img/d.png)

Arrancando el proyecto:

![picture](img/e.png)

### Autenticacion:

![picture](img/f.png)

Ingreso con usuario ccaycedo y clave ccaycedo encriptacion MD5:

![picture](img/g.png)

### Listado de encuestas:

![picture](img/H.png)

Diligenciando una nueva encuesta:

![picture](img/i.png)

Aqui vemos la encuesta ya diligenciada:

![picture](img/k.png)

### Crear y eliminar marcas:

![picture](img/l.png)

Opcion para crear una nueva marca de PC:

![picture](img/r.png)

### Crear y eliminar usuarios:

![picture](img/m.png)

Opcion para crear un nuevo usuario:

![picture](img/s.png)

Cerrar la sesion:

![picture](img/n.png)

# Servicio RESTFUL en SpringBoot

Consulta publica de encuestas mediante un servicio REST:

![picture](img/o.png)

Probando el servicio RESTFUL:

![picture](img/p.png)

# Validar session al intentar ingresar por URL

![picture](img/T.png)

# Validacion de campos

![picture](img/x.png)

# Fecha generada por el sistema

![picture](img/z.png)
