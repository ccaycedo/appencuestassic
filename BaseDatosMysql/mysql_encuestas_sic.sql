

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";



--
-- Base de datos: `encuestas_sic`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `encuesta`
--

DROP TABLE IF EXISTS `encuesta`;
CREATE TABLE IF NOT EXISTS `encuesta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cedula` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `comentarios` varchar(5000) NOT NULL,
  `idMarcas` int(11) NOT NULL,
  `desMarca` varchar(50) NOT NULL,
  `idUsuario` int(11) NOT NULL,
  `desUsuario` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `estado` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `encuesta`
--

INSERT INTO `encuesta` (`id`, `cedula`, `email`, `comentarios`, `idMarcas`, `desMarca`, `idUsuario`, `desUsuario`, `fecha`, `estado`) VALUES
(14, 1018415735, 'ccaycedo2@gmail.com', 'Me gustan los PC', 1, 'DELL', 1, 'ccaycedo', '2019-12-09', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `marcas`
--

DROP TABLE IF EXISTS `marcas`;
CREATE TABLE IF NOT EXISTS `marcas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(100) NOT NULL,
  `estado` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `marcas`
--

INSERT INTO `marcas` (`id`, `descripcion`, `estado`) VALUES
(1, 'DELL', 1),
(2, 'Lenovo', 1),
(3, 'Mac', 1),
(4, 'HP', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

DROP TABLE IF EXISTS `usuario`;
CREATE TABLE IF NOT EXISTS `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `login` varchar(50) NOT NULL,
  `clave` varchar(50) NOT NULL,
  `rol` int(11) NOT NULL,
  `estado` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id`, `nombre`, `login`, `clave`, `rol`, `estado`) VALUES
(1, 'Cristhian Caycedo', 'ccaycedo', 'cbf9bf2599f3c562349d134fda27807d', 1, 1),
(2, 'Maria Neiva', 'mneiva', 'd7575c8fc9270ad168caf67226c1cf3a', 1, 1),
(3, 'camilo', 'camilo', '0aa0b6b3207f0b3839381db1962574a2', 0, 0),
(4, 'juan', 'juan', 'a94652aa97c7211ba8954dd15a3cf838', 0, 0);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
