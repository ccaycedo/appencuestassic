package com.sic.encuesta.encuestaREST;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EncuestaRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(EncuestaRestApplication.class, args);
	}

}
