package com.sic.encuesta.encuestaREST;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;



@CrossOrigin(origins = "http://localhost:4200",maxAge = 3600)
@RestController
@RequestMapping({"/encuestasRest"})
public class Servicio {
	
	@Autowired
	EncuestaRepositorio repositorio;
	
	@GetMapping
	public List<Encuesta> listar() {
		return (List<Encuesta>) repositorio.findAll();		
		
	}

}
