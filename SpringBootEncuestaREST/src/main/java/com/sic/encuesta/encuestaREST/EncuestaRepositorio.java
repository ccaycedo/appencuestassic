package com.sic.encuesta.encuestaREST;

import org.springframework.data.repository.CrudRepository;

public interface EncuestaRepositorio extends CrudRepository<Encuesta, Integer> {

}
