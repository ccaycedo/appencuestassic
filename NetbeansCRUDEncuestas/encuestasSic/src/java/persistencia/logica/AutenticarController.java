/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia.logica;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author ALEJANDROTERCERO88
 */
@WebServlet(name = "AutenticarController", urlPatterns = {"/AutenticarController"})
public class AutenticarController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String login = request.getParameter("login");
            String clave = request.getParameter("clave");
            String claveEncriptada = getMD5(clave);
            
            out.print(login+"  "+claveEncriptada+" "+clave);
            
            UsuarioController controlador=new UsuarioController();
            
            if(autenticarUsuario(login, claveEncriptada)){
                HttpSession misession= request.getSession(true);
                misession.setAttribute("usuario",login);
                
                response.sendRedirect("/encuestasSic/faces/presentacion/encuesta/List.xhtml"); 
                //out.print("SIII");
            }else{
                response.sendRedirect("/encuestasSic/"); 
                //out.print("NOOO");
            }
            //out.print(claveEncriptada);
            
        }catch(Exception e){
            response.sendRedirect("/encuestasSic/"); 
        }
    }
    
    public String getMD5(String input) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] messageDigest = md.digest(input.getBytes());
            BigInteger number = new BigInteger(1, messageDigest);
            String hashtext = number.toString(16);

            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }
            return hashtext;
        }
        catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    public static Connection obtener() throws SQLException,  ClassNotFoundException {
       Connection cnx;
           try {
              Class.forName("com.mysql.jdbc.Driver");
              cnx = DriverManager.getConnection("jdbc:mysql://localhost/encuestas_sic", "root", "");
           } catch (SQLException ex) {
              throw new SQLException(ex);
           } catch (ClassNotFoundException ex) {
              throw new ClassCastException(ex.getMessage());
           }
        
        return cnx;
    }
    
    public boolean autenticarUsuario(String u,String c) throws SQLException, ClassNotFoundException{
        
        boolean ok=false;
        Connection cnx=obtener();
            
       
        String query = "SELECT * FROM usuario where login like '%"+u+"%' and clave like '%"+c+"%'";      
        Statement st = cnx.createStatement();      
        ResultSet rs = st.executeQuery(query);
        
      
        if (rs.next())
        {
            ok=true;
        }
       
        
        return ok;
        
    }
    
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
